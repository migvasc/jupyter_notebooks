def load_co2(path):

    month_days = { 1:31,
        2:28,
        3:31,
        4:30,
        5:31,
        6:30,
        7:31,
        8:31,
        9:30,
        10:30,
        11:30,
        12:31
    }
    
    co2_array = [0]
    month = 1
    with open(path, encoding="UTF-8") as workload_file:    
        cursor = 0
        for line_number, line in enumerate(workload_file):                        
            co2 = float(line.replace('\n',''))
            for i in range(0,month_days[month]):
                co2_array.append(co2)
            month+=1
    return co2_array


path = '/home/migvasc/Desktop/OLD LAPTOP/PHD/notebooks/input/co_2/sp.csv'
array = load_co2(path)
print(array,len(array))
