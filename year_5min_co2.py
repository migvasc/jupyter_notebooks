from pulp import *
import time


class Util:
    #### Util functions
    @staticmethod
    def load_workload(path):
        workload_array = [0]
        for i in range(0,366):
            with open(path, encoding="UTF-8") as workload_file:    
                cursor = 0
                for line_number, line in enumerate(workload_file):
                    if line_number == 0:
                        continue
                    row = line.split(',')
                    ncores = int(row[1].replace('\n',''))
                    
                    workload_array.append(ncores)
        print(len(workload_array))
        return workload_array

    @staticmethod
    def load_co2(path):
        # actually, it starts in august
        month_days = { 1:31,
            2:30,
            3:30,
            4:30,
            5:31,
            6:31,
            7:28,
            8:31,
            9:30,
            10:31,
            11:30,
            12:31
        }
        
        co2_array = [0]
        month = 1
        with open(path, encoding="UTF-8") as workload_file:    
            cursor = 0
            for line_number, line in enumerate(workload_file):                        
                co2 = float(line.replace('\n',''))
                for day in range(0,month_days[month]):
                    for hour in range(0,24):
                        
                        co2_array.append(co2)
                month+=1
                if month==13:
                    break
        return co2_array

    @staticmethod
    def load_irradiance(path):    
        irradiation_array = [0]
        with open(path, encoding="UTF-8") as irradiation_file:    
            cursor = 0
            for line_number, line in enumerate(irradiation_file):
                if line_number <= 24: # First 24 lines are description of the input
                    continue

                row = line.split(';')
                if(len(row)<10): # The csv has 10 columns, this condition is only valid at the end of the file
                    break
                solar_irradiation = float(row[10].replace('\n',''))
                irradiation_array.append(solar_irradiation)                
        return irradiation_array

    @staticmethod
    def extract_dict_variables(prob):
        variables = {}
        # Each of the variables is printed with it's resolved optimum value
        for v in lpProb.variables():
            variables[v.name] = v.varValue    
        return variables    

class GreenCloudLP:    

    def __init__(self, name, timeslots, DCs, workload, irradiance, C, pv_co2, grid_co2, pIdleDC, pNetIntraDC, PUE, pCore, bat_co2, eta_ch, eta_dch, eta_pv):
        
        """
        Initialize the LP model with the inputs.
        :param str name: Name of the problem
        :param timeslots: List containing the time slots
        :param DCs: List containing the name of the Data Centers (DC)
        :param workload: List containing the workload that will be executed at each time slot (number of CPU cores)
        :param irradiance: Dictionary, where the key is the DC name and the value is a list with solar irradiation values
        :param C: Dictionary, where the key is the DC name and the value is a list with the CPU cores capacity
        :param pv_co2: Dictionary, where the key is the DC name and the value is the emissions from pvs based in the DC location (g co2 / kWh )
        :param grid_co2: Dictionary, where the key is the DC name and the value is the emissions from local electricity grid based in the DC location
        :param pIdleDC: Dictionary, where the key is the DC name and the value is how much watts its servers consume when IDLE   
        :param pNetIntraDC: Dictionary, where the key is the DC name and the value is how much watts its network equipment consume when IDLE
        :param PUE: The data center Power Usage Effectiveness value
        :param pCore: Dynamic costs of computing a server
        :param bat_co2: Emissions of manufacturing the battery (g co2 / kWh of capacity)
        :param eta_ch: The effiency of the charging process into the batteries
        :param eta_dch: The effiency of the discharging process into the batteries
        :param eta_pv:  The effiency of converting solar irradiation into energy in the PVs   
        
        """
        self.name = name
        self.timeslots = timeslots
        self.DCs = DCs
        self.workload = workload
        self.irradiance = irradiance
        self.C = C
        self.pv_co2 =  pv_co2
        self.grid_co2 = grid_co2
        self.pIdleDC = pIdleDC
        self.pNetIntraDC = pNetIntraDC
        self.PUE = PUE
        self.pCore = pCore
        self.bat_co2= bat_co2
        self.eta_ch = eta_ch
        self.eta_dch= eta_dch
        self.eta_pv =eta_pv
        
        self.DCS_UTC = {        
           "DC_CANBERRA":10, 
           "DC_SEOUL":9, 
           "DC_PARIS":2, 
           "DC_VIRGINIA":-4,
           "DC_DUBAI":4,
           "DC_SINGAPORE":8, 
           "DC_PUNE":6, 
           "DC_JOHANNESBURG": 2, 
           "DC_SP": -3
        }

        
        
        self.dcs_instants_midnight = {}

        for dc in self.DCs:
            self.dcs_instants_midnight[dc] = [ (i * 24) -self.DCS_UTC[dc] for i in range(1,365) ] 
        
        
        
        ### Variables    
        self.A   = LpVariable.dicts("A",self.DCs, 0,    cat='Continuous')                           # PV panels area (m²)
        self.Bat = LpVariable.dicts("Bat",self.DCs, 0, cat='Continuous')                            # Battery capacity in Wh
        self.B  = LpVariable.dicts('B_', (self.DCs,self.timeslots),lowBound=0, cat='Continuous')     # Level of energy
        self.Pdch = LpVariable.dicts('Pdch_', (self.DCs,self.timeslots),lowBound=0,cat='Continuous') # Power to discharge (W)
        self.Pch = LpVariable.dicts('Pch_', (self.DCs,self.timeslots),lowBound=0, cat='Continuous')  # Power to charge (W)
        self.w = LpVariable.dicts('w_', (self.DCs,self.timeslots),lowBound=0, cat='Continuous')  # workload to be sent to each DC
        self.Pgrid = LpVariable.dicts('Pgrid_', (self.DCs,self.timeslots),lowBound=0, cat='Continuous')     # Green power surplus sold back to the grid
        self.loe = LpVariable.dicts("loe_", DCs, 0,    cat='Continuous')                     # DC Level of energy
        
        
    ### Auxiliary functions
    def getDCPowerConsumption(self,d,k):
        if(k ==0):
            return 0
        return self.PUE * (self.pNetIntraDC[d]+ self.pIdleDC[d]  + self.w[d][k] * self.pCore ) * 1/1000
    
    def FPgrid(self,d,k):
        return self.grid_co2[d][k]  * self.Pgrid[d][k] 

    def FPpv(self,d,k):
        return self.Pre(d,k) * self.pv_co2[d]

    def FPbat(self,d):
        return self.Bat[d] * self.bat_co2

    def P(self,d,k):
        return self.getDCPowerConsumption(d,k) 

    def Pre(self,d,k):
        return (self.A[d] * self.irradiance[d][k] * self.eta_pv) * 1/1000 # convert to kw

    def use_original_objective_function(self,prob):    
        prob +=  lpSum(
            [  self.FPgrid(d,k)  + self.FPpv(d,k)  for k in self.timeslots ]  + self.FPbat(d) for d in self.DCs)  
        return prob        
        
    def use_Pdch_obj_function(self,prob):    
        prob +=  lpSum(
            [  self.FPgrid(d,k)  + self.FPpv(d,k) for k in self.timeslots ]  + self.FPbat(d) for d in self.DCs)  
        return prob

    def use_loe_objective_function(self,prob):
        for d in self.DCs:
            prob +=  lpSum([  self.B[d][i] for i in self.dcs_instants_midnight[d] ]  ) ==self.loe[d]

        ##  OBJECTIVE FUNCTION
        prob +=  lpSum(
            [  self.FPgrid(d,k)  + self.FPpv(d,k)  for k in self.timeslots ]  + self.FPbat(d) - self.loe[d]*0.0000000001 for d in self.DCs)  
        return prob

    
    def build_lp(self):        
        prob = LpProblem(self.name, LpMinimize)

        for d in self.DCs:
            prob.addConstraint( self.Pch[d][0]   == 0.0 )
            prob.addConstraint( self.Pdch[d][0]   == 0.0 )

        for d in self.DCs:
            for k in timeslots[1:] :
                prob.addConstraint( self.B[d][k]  == self.B[d][k-1] + self.Pch[d][k]*self.eta_ch  - self.Pdch[d][k]*self.eta_dch )
                prob.addConstraint( self.Pch[d][k]  * self.eta_ch <= 0.8 * self.Bat[d] - self.B[d][k-1] )   # To ensure that only charge/discharge if
                prob.addConstraint( self.Pdch[d][k] * self.eta_dch <= self.B[d][k-1] -  0.2 * self.Bat[d] ) # battery capacity >= 0 


        for d in self.DCs:
            for k in self.timeslots :
                prob.addConstraint( self.P(d,k) <= self.Pgrid[d][k] + self.Pre(d,k) + self.Pdch[d][k] - self.Pch[d][k]  )
                prob.addConstraint( self.FPgrid(d,k) >= 0 )
                prob.addConstraint( self.Pre(d,k) >= self.Pch[d][k]   )
                prob.addConstraint( self.B[d][k]   >= 0.2 * self.Bat[d] )
                prob.addConstraint( self.B[d][k]   <= 0.8 * self.Bat[d] )
                prob.addConstraint( self.w[d][k]<=self.C[d])

        for k in self.timeslots:
                prob +=  lpSum([  self.w[d][k]   for d in self.DCs]) == self.workload[k]    
        
        return prob

PUE = 1.20
pCore  = 10.25 #Values in W

DCS_UTC = {        
       "DC_CANBERRA":10, 
       "DC_SEOUL":9, 
       "DC_PARIS":2, 
       "DC_VIRGINIA":-4,
       "DC_DUBAI":4,
       "DC_SINGAPORE":8, 
       "DC_PUNE":6, 
       "DC_JOHANNESBURG": 2, 
       "DC_SP": -3
}

pIdleDC  = {
       "DC_CANBERRA":449110, 
       "DC_SEOUL":449110, 
       "DC_PARIS":449110, 
       "DC_VIRGINIA":449110,
       "DC_DUBAI":449110,
       "DC_SINGAPORE":449110, 
       "DC_PUNE":449110, 
       "DC_JOHANNESBURG": 449110, 
       "DC_SP": 449110 } # Watts

C = {"DC_CANBERRA":55560,
       "DC_SEOUL":55560, 
       "DC_PARIS":55560,
       "DC_VIRGINIA":55560,
       "DC_DUBAI":55560,
       "DC_SINGAPORE":55560,
       "DC_PUNE":55560,
       "DC_JOHANNESBURG":55560,
       "DC_SP":55560}

pNetIntraDC = {
       "DC_CANBERRA":149760, 
       "DC_SEOUL":149760, 
       "DC_PARIS":149760, 
       "DC_VIRGINIA":149760,
       "DC_DUBAI":149760,
       "DC_SINGAPORE":149760, 
       "DC_PUNE":149760, 
       "DC_JOHANNESBURG":149760, 
       "DC_SP":149760 } # Watts

# Create timeslots

length_k = 8761
timeslots = list(range(length_k))

workload_file ='/home/migvasc/Desktop/OLD LAPTOP/PHD/notebooks/input/workload_group_h.csv'
#solar_irradiance_file_path = '/home/migvasc/Desktop/PV_POWER'
solar_irradiance_file_path = '/home/migvasc/Desktop/OLD LAPTOP/PHD/notebooks/input/solar_irradiation_data/year/'
co2_file_path = '/home/migvasc/Desktop/OLD LAPTOP/PHD/notebooks/input/solar_irradiation_data/year/'

workload = Util.load_workload(workload_file)

DCs = ["DC_CANBERRA", "DC_SEOUL", "DC_PARIS", "DC_VIRGINIA","DC_DUBAI", "DC_SINGAPORE", "DC_PUNE", "DC_JOHANNESBURG", "DC_SP"]

dcs_instants_midnight = {}

for dc in DCs:
    dcs_instants_midnight[dc] = [ (i * 24) -DCS_UTC[dc] for i in range(1,365) ] 

grid_co2 =  {"DC_CANBERRA":Util.load_co2(co2__file_path+'/canberra.csv') , 
       "DC_SEOUL":Util.load_co2(co2__file_path+'/seoul.csv'), 
       "DC_PARIS":Util.load_co2(co2__file_path+'/paris.csv'), 
       "DC_VIRGINIA":Util.load_co2(co2__file_path+'/virginia.csv'),
       "DC_DUBAI":Util.load_co2(co2__file_path+'/dubai.csv'),
       "DC_SINGAPORE":Util.load_co2(co2__file_path+'/singapore.csv'), 
       "DC_PUNE":Util.load_co2(co2__file_path+'/pune.csv') , 
       "DC_JOHANNESBURG":Util.load_co2(co2__file_path+'/johannesburg.csv'), 
       "DC_SP":Util.load_co2(co2__file_path+'/sp.csv') }

pv_co2=  {
       "DC_CANBERRA":28.44 , 
       "DC_SEOUL":30.36 , 
       "DC_PARIS":36.13 , 
       "DC_VIRGINIA":28.4 ,
       "DC_DUBAI":22.72 ,
       "DC_SINGAPORE":34.98 , 
       "DC_PUNE":25.24 , 
       "DC_JOHANNESBURG":22.96 , 
       "DC_SP":25.92  }

bat_co2  = 0.673515982 * (length_k-1)  #5900  # g co2 eq/ kwh of bat capacity
print(bat_co2)
        
irradiance = {"DC_CANBERRA":Util.load_irradiance(solar_irradiance_file_path+'/canberra/2021.csv'), 
       "DC_SEOUL":Util.load_irradiance(solar_irradiance_file_path+'/seoul/2021.csv'),
       "DC_PARIS":Util.load_irradiance(solar_irradiance_file_path+'/paris/2021.csv'),
       "DC_VIRGINIA":Util.load_irradiance(solar_irradiance_file_path+'/virginia/2021.csv'),
       "DC_DUBAI":Util.load_irradiance(solar_irradiance_file_path+'/dubai/2021.csv'),
       "DC_SINGAPORE":Util.load_irradiance(solar_irradiance_file_path+'/singapore/2021.csv'), 
       "DC_PUNE":Util.load_irradiance(solar_irradiance_file_path+'/pune/2021.csv'), 
       "DC_JOHANNESBURG":Util.load_irradiance(solar_irradiance_file_path+'/johannesburg/2021.csv'), 
       "DC_SP":Util.load_irradiance(solar_irradiance_file_path+'/sp/2021.csv') }


eta_ch = .85
eta_dch = 1/eta_ch
eta_pv  = 0.15 #15%
dch_factor = 1

### SOLVER WILL BE GUROBI
solver = GUROBI_CMD()

# The inputs will be the same as before, so there is no need to pass them again

lpModel = GreenCloudLP("Green_DC_Pdch_5min", timeslots, DCs, workload, irradiance, C, pv_co2, grid_co2, pIdleDC, pNetIntraDC, PUE, pCore, bat_co2, eta_ch, eta_dch, eta_pv)
lpProb = lpModel.build_lp()
lpProb = lpModel.use_Pdch_obj_function(lpProb)
    
# The problem data is written to an .lp file
lpProb.writeLP("GreenDC_Pdch_multiple_co2.lp")


start = time.time()
# The problem is solved using PuLP's choice of Solver
lpProb.solve(solver)
# The status of the solution is printed to the screen
print("Status:", LpStatus[lpProb.status])
    
# The optimised objective function value is printed to the screen
print("Total emissions of the cloud = ", value(lpProb.objective))
end = time.time()

print(f"Executed in {end - start} s")


dict_variables_Pdch = Util.extract_dict_variables(lpProb)
for dc in DCs:
    print("Bat_"+ dc,dict_variables_Pdch["Bat_"+ dc])
    print("A_"+ dc,  dict_variables_Pdch["A_"+ dc])

cont = 0
for k in timeslots:
    for d in DCs:
        if dict_variables_Pdch['Pdch__'+d+'_'+str(k)]>= 1 and dict_variables_Pdch['Pch__'+d+'_'+str(k)]>= 1:
            cont+=1
            
print("Charge and Discharge", cont)
variables = dict_variables_Pdch




