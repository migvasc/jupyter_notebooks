from cmath import e
from operator import le
from pulp import *
import time
import sys


### Util functions

def load_workload(path):
    workload_array = [0]
    for i in range(0,366):
        with open(path, encoding="UTF-8") as workload_file:    
            cursor = 0
            for line_number, line in enumerate(workload_file):
                if line_number == 0:
                    continue
                row = line.split(',')
                ncores = int(row[1].replace('\n',''))
                workload_array.append(ncores)
    return workload_array

def load_irradiance(path):
    irradiation_array = [0]
    with open(path, encoding="UTF-8") as irradiation_file:    
        cursor = 0
        for line_number, line in enumerate(irradiation_file):
            if line_number <= 24: # First 24 lines are description of the input
                continue

            row = line.split(';')
            if(len(row)<10): # The csv has 10 columns, this condition is only valid at the end of the file
                break
            solar_irradiation = float(row[10].replace('\n',''))
            irradiation_array.append(solar_irradiation)
    return irradiation_array




def runLP(i,j):

    start = time.time()

    # Create the 'prob' variable to contain the problem data
    prob = LpProblem("Green DC", LpMinimize)

    ## INPUTS  

    # Create timeslots
    length_k = (j-i) 
    timeslots = list(range(length_k)) # from 0 to 4
    workload_file ='/home/migvasc/Desktop/OLD LAPTOP/PHD/notebooks/input/workload_group_h.csv'
    solar_irradiance_file_path = '/home/migvasc/Desktop/OLD LAPTOP/PHD/notebooks/input/solar_irradiation_data/'
    workload = load_workload(workload_file)

    DCs = ["DC_CANBERRA", "DC_SEOUL", "DC_PARIS", "DC_VIRGINIA","DC_DUBAI", "DC_SINGAPORE", "DC_PUNE", "DC_JOHANNESBURG", "DC_SP"]

    C = {"DC_CANBERRA":54984, 
        "DC_SEOUL":35076, 
        "DC_PARIS":18012, 
        "DC_VIRGINIA":48822,
        "DC_DUBAI":113760,
        "DC_SINGAPORE":20856, 
        "DC_PUNE":61146, 
        "DC_JOHANNESBURG":71574, 
        "DC_SP":66360 }


    grid_co2 =  {"DC_CANBERRA":667.0, 
        "DC_SEOUL":415.6, 
        "DC_PARIS":52.6, 
        "DC_VIRGINIA":342.8,
        "DC_DUBAI":530.0,
        "DC_SINGAPORE":495.0, 
        "DC_PUNE":702.8, 
        "DC_JOHANNESBURG":900.6, 
        "DC_SP":61.7 }

    pv_co2=  {"DC_CANBERRA":28.44, 
        "DC_SEOUL":30.36, 
        "DC_PARIS":36.13, 
        "DC_VIRGINIA":28.4,
        "DC_DUBAI":22.72,
        "DC_SINGAPORE":34.98, 
        "DC_PUNE":25.24, 
        "DC_JOHANNESBURG":22.96, 
        "DC_SP":25.92 }

    bat_co2  = 2950 # g co2 eq/ kwh of bat capacity

    pCore = 0.03666 # 0.03666 # 6 cores = 220 W, 1 core ≃ 36.77W
    # twice the power consumption to see if it will make any difference...

    irradiance = {"DC_CANBERRA":load_irradiance(solar_irradiance_file_path+'/year/canberra.csv')[i:j], 
        "DC_SEOUL":load_irradiance(solar_irradiance_file_path+'/year/seoul.csv')[i:j],
        "DC_PARIS":load_irradiance(solar_irradiance_file_path+'/year/paris.csv')[i:j],
        "DC_VIRGINIA":load_irradiance(solar_irradiance_file_path+'/year/virginia.csv')[i:j],
        "DC_DUBAI":load_irradiance(solar_irradiance_file_path+'/year/dubai.csv')[i:j],
        "DC_SINGAPORE":load_irradiance(solar_irradiance_file_path+'/year/singapore.csv')[i:j], 
        "DC_PUNE":load_irradiance(solar_irradiance_file_path+'/year/pune.csv')[i:j], 
        "DC_JOHANNESBURG":load_irradiance(solar_irradiance_file_path+'/year/johannesburg.csv')[i:j], 
        "DC_SP":load_irradiance(solar_irradiance_file_path+'/year/sp.csv')[i:j] }

    eta_ch = .85
    eta_dch = 1/eta_ch
    eta_pv  = 0.15 #15%

    ## Variables

    A   = LpVariable.dicts("A",DCs, 0,    cat='Continuous')                           # PV panels area (m²)
    Bat = LpVariable.dicts("Bat",DCs, 0, cat='Continuous')                            # Battery capacity in Wh
    B  = LpVariable.dicts('B_', (DCs,timeslots),lowBound=0, cat='Continuous')     # Level of energy
    Pdch = LpVariable.dicts('Pdch_', (DCs,timeslots),lowBound=0,cat='Continuous') # Power to discharge (W)
    Pch = LpVariable.dicts('Pch_', (DCs,timeslots),lowBound=0, cat='Continuous')  # Power to charge (W)
    w = LpVariable.dicts('w_', (DCs,timeslots),lowBound=0, cat='Continuous')  # workload to be sent to each DC
    Pgrid = LpVariable.dicts('Pgrid_', (DCs,timeslots),lowBound=0, cat='Continuous')     # Green power surplus sold back to the grid


    ## Auxiliary functions
    def FPgrid(d,k):
        return grid_co2[d] * Pgrid[d][k]
                                    
    def FPpv(d,k):
        return Pre(d,k) * pv_co2[d]

    def FPbat(d):
        return Bat[d] * bat_co2

    def P(d,k):
        return w[d][k] * pCore

    def Pre(d,k):
        return (A[d] * irradiance[d][k] * eta_pv) / 1000 # convert to kw



    ##  OBJECTIVE FUNCTION

    prob +=  lpSum(
        [  FPgrid(d,k)  + FPpv(d,k)   for k in timeslots ]  + FPbat(d)  for d in DCs)  


    ##  RESTRICTIONS
    for d in DCs:
        prob.addConstraint( B[d][0]   == 0.2*Bat[d] )
        prob.addConstraint( Pch[d][0]   == 0.0 )
        prob.addConstraint( Pdch[d][0]   == 0.0 )

    for d in DCs:
        for k in timeslots[1:] :
            prob.addConstraint( B[d][k]  == B[d][k-1] + Pch[d][k]*eta_ch  - Pdch[d][k]*eta_dch )
            prob.addConstraint( Pch[d][k]  * eta_ch <= 0.8 * Bat[d] - B[d][k-1] ) # To ensure that only charge/discharge if
            prob.addConstraint( Pdch[d][k] * eta_dch <= B[d][k-1] -  0.2 * Bat[d] ) # battery capacity >= 0 

    for d in DCs:
        for k in timeslots :
            prob.addConstraint( P(d,k) <= Pgrid[d][k] + Pre(d,k) + Pdch[d][k] - Pch[d][k]  )
            prob.addConstraint( FPgrid(d,k) >= 0 )
            prob.addConstraint( Pre(d,k) >= Pch[d][k]   )
            prob.addConstraint( B[d][k]   >= 0.2 * Bat[d] )
            prob.addConstraint( B[d][k]   <= 0.8 * Bat[d] )
            prob.addConstraint( w[d][k]<=C[d])
        

    for k in timeslots:
            prob +=  lpSum([  w[d][k]   for d in DCs]) == workload[k]


    # The problem data is written to an .lp file
    prob.writeLP("GreenDC.lp")

    # The problem is solved using PuLP's choice of Solver
    prob.solve()


    # The status of the solution is printed to the screen
    print("Status:", LpStatus[prob.status])

    variables_pgrid = {}

    # Each of the variables is printed with it's resolved optimum value
    for v in prob.variables():
        variables_pgrid[v.name] = v.varValue


    # The optimised objective function value is printed to the screen
    print("Total emissions of the cloud = ", value(prob.objective))

    end = time.time()
    print(f"Executed in {end - start} s")

    for dc in DCs:
        print("Bat_"+ dc,variables_pgrid["Bat_"+ dc])
        print("A_"+ dc,variables_pgrid["A_"+ dc])
        
    print(f'length_k {length_k}')
    cont = 0
    for k in timeslots:
        for d in DCs:
            if variables_pgrid['Pdch__'+d+'_'+str(k)]>= 1 and variables_pgrid['Pch__'+d+'_'+str(k)]>= 1:
                print( variables_pgrid['Pdch__'+d+'_'+str(k)], variables_pgrid['Pch__'+d+'_'+str(k)] )
                cont+=1
    print(f'total charge and discharge {cont}')

print (sys.argv)
start = int(sys.argv[1])
end =   int(sys.argv[2])
runLP(start,end)
print (start, end)
